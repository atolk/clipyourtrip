/// <reference types="mocha" />
/// <reference types="chai" />

// import chai = require('chai');
 
// (function () {
    describe('<cat-rating>', function() { 

        
    
        let el: CatRating;

        beforeEach(function(){
            console.log('flushing all');
            el = fixture('testFixture');
        });

        it('По-умолчанию должен иметь параметры 0 из 5', function(done){

            flush(function() {
                //- let nodes = el.shadowRoot.querySelectorAll('i');
                chai.expect(el.max).to.be.equal(5);
                chai.expect(el.rating).to.be.equal(0);

                done();
            });
        });


        it('Элемент должен отрисовывать заданное количество иконок', function(done){
            el.rating = 4;
            el.max = 5;

            flush(function() {
                let nodes = el.shadowRoot.querySelectorAll('i');
                chai.expect(nodes.length).to.be.equal(5);
                done();
            });
        });

        it('Должен проставлять класс active только у заданного количества звезд', function(done){
            el.rating = 12;
            el.max = 15;

            flush(function() {
                let nodes = el.shadowRoot.querySelectorAll('i[active]');
                chai.expect(nodes.length).to.be.equal(12);
                done();
            });

        });

    });
// }());