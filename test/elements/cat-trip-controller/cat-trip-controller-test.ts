/// <reference types="mocha" />
/// <reference types="chai" />

// import chai = require('chai');
 
// (function () {
    describe('<cat-trip-controller>', function() { 

        let mockDataProvider: CatDataProvider.TripDataProvider = {
            getUserTripsMeta : function () {throw new Error('Not implemented');},
            getUserTrip: function(uid: string) {throw new Error('Not implemented');},
            deleteTrip: function(uid: string): Promise<string> {throw new Error('Not implemented');},
            pushTrip: function(trip: CatBase.Trip): Promise<string> {throw new Error('Not implemented');}
        };
        

        
    
        let el: CatTripController;

        beforeEach(function(){
            console.log('flushing all');
            el = fixture('testFixture');
        });

        it('Проверочка', function() {

            flush(function() {
                chai.expect('local').to.be.equal('local');
            }); // flush

        }); // it


        /****************************************************************************************** */
        describe("Работа с массивом активностей", function() {
            /**************************************************************/
            describe("Неверные параметры функции", function(){ 
                it('Даты равны должен выбрасывать исключение', function(){
                    let f = function() { el._configureTripDays(new Date(), new Date()); };
                    chai.expect(f).to.throw(Error);
                });
                // Эти кейсы вполне могут быть, т.к. путешествия могут загружены из репозитория
                // it('Начало поездки вчера. Должен выбрасывать исключение', function(){
                //     let f = function() { tc._configureTripDays(dateFns.addDays(new Date(), -5), new Date()); };
                //     expect(f).to.throw(Error);
                // });

                // it('Окончание поездки вчера. Должен выбрасывать исключение', function(){
                //     let f = function() { tc._configureTripDays(new Date(), dateFns.addDays(new Date(), -5)); };
                //     expect(f).to.throw(Error);
                // });
                it('Окончание раньше начала. Должен выбрасывать исключение', function(){
                    let f = function() { el._configureTripDays(dateFns.addDays(new Date(), 5), dateFns.addDays(new Date(), 3)); };
                    chai.expect(f).to.throw(Error);
                });
            });
            /****************************************************************************************** */
            describe('Работа с существующим массивом', () => {
                it('Инициализация нового массива', function(){
                    let f = el._configureTripDays;
                    chai.expect(f(new Date(), dateFns.addDays(new Date(), 12)))
                        .to.have.lengthOf(13, 'неправильное количество элементов массива');
                });
                it('Сохранение исходных элементов при расширении массива', function(){
                    let f = el._configureTripDays;
                    let days = f(new Date, dateFns.addDays(new Date(), 7));

                    let newDays = f(new Date(), dateFns.addDays(new Date, 14), days);
                    chai.expect(newDays.slice(0, days.length))
                        .to.deep.equal(days);
                });

                it('Удаление элементов массива', function(){
                    let f = el._configureTripDays;
                    let days = f(new Date, dateFns.addDays(new Date(), 7));
                    
                    let newDays = f(new Date(), dateFns.addDays(new Date, 3), days);
                    chai.expect(newDays)
                        .to.deep.equal(days.slice(0, newDays.length));
                });
        
            });
        });
    }); // describe
// }());