/// <reference types="mocha" />
/// <reference types="chai" />

// import chai = require('chai');
 
// (function () {
    describe('<cat-map>', function() { 

        
    
        let el: CatMap;

        beforeEach(function(){
            console.log('flushing all');
            el = fixture('testFixture');
        });

        it('Зум через пограничное значение должен вызывать событие', function(done) {

            flush(function() {
                console.log(el);

                el.addEventListener('cat-map-mode-changed', (e: CustomEvent) => {
                            
                            chai.expect(e.detail.mode).to.be.equal('local');
                            alert(e.detail.mode);
                            done();
                        });

                el.ensureInit()
                    .then(googleMap => {
                        googleMap.setZoom(3);
                        console.log(el);
                        googleMap.setZoom(8);
                    });
            }); // flush

        }); // it
    }); // describe
// }());