/// <reference types="mocha" />
/// <reference types="chai" />

describe('search criterias', function() {

    var provider: CatDataProvider.SearchDataProvider;
    
    beforeEach(function(){
        provider = new CatDataProvider.CatProviderSearch();
    });

    it('должен отдавать любой массив со странами заданной структуры', function(){
        return provider.getCountryList('Б')
                .then(res => {
                    //alert();
                    chai.expect(res.length).is.above(0);
                    res.forEach(el => {
                        chai.expect(el).is.instanceof(CatSearch.Criteria);
                    });
                });
    });

    it('Ищем конкретную страну и она одна такая', function(){
        return provider.getCountryList('БеЛаРус')
                .then(res => {
                    chai.expect(res.length).is.equal(1);
                });
    });

    it('Должен корректно обрабатывать и не валиться на всяком мусоре', function(){
        return provider.getCountryList('sdf saf sa df sf sad %% %  % f&^^ 1234 23408 ')
                .then(res => {
                    chai.expect(res.length).is.equal(0);
                });
    });

    it('Несколько запросов на одном экземпляре класса должны возвращать корректные результаты', function(){
        return provider.getCountryList('sdf saf sa df sf sad %% %  % f&^^ 1234 23408 ')
                .then(res => {
                    chai.expect(res.length).is.equal(0);
                    return provider.getCountryList('Бел')
                        .then(res => chai.expect(res.length).above(0));
                });
    });

});