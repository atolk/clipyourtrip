declare var Pikaday: any;

class CatPikaday extends Polymer.Element {
    static get is() { return 'cat-pikaday';}

    $: {
        startField: HTMLElement,
        endField: HTMLElement,
        startContainer: HTMLElement,
        endContainer: HTMLElement,
    };

    static get properties() { return {
        startDate: {
            type: String,
            notify: true,
        },

        endDate: {
            type: String,
            notify: true,
        },

        dayAndNight: {
            type: String,
            notify: true,
            computed: '_getDayAndNight(startDate, endDate)'
        },

        rangeValue: {
            type: String, 
            computed: '_getRangeValue(startDate, endDate)',
        }
    }} // properties

    connectedCallback() {
        super.connectedCallback();

        // Инициализация всего.
        this._mdcStartField = new mdc.textfield.MDCTextfield(this.$.startField.parentElement);
        this._mdcEndField = new mdc.textfield.MDCTextfield(this.$.endField.parentElement);
        this.initFields();
    }

    // 4TS
    private _mdcStartField: any;
    private _mdcEndField: any;
    private _startPicker: any;
    private _endPicker: any;
    public startDate: Date;
    public endDate: Date;

    _getDayAndNight(startDate: Date, endDate: Date): string {
        if (!(startDate && endDate)) return;
        //console.log('Пикеры: ', this._endPicker.getDate(), this._startPicker.getDate());
        //let nCount = dateFns.differenceInCalendarDays(this._endPicker.getDate(), this._startPicker.getDate());
        let nCount = dateFns.differenceInCalendarDays(endDate, startDate);
        return `${nCount + 1} дн. (${nCount} ноч.)`;
    }

    _getRangeValue(startDate: Date, endDate: Date): string {
        return `${startDate} = ${endDate}`;
    }

    show() {
        if (!this._startPicker)
            this.initFields();
        this._startPicker.show();
    }


    // Фокус на поле стартовой даты. Запускаем процесс.
    _startFieldFocused(e: Event) {
        console.log(e);
        this.show();
    }

    // Фокус на поле даты окончания.
    _endFieldFocused(e: Event) {
        // Если пикер не инициализирован, то запускаем процесс с самого начала.
        if (!this._endPicker) {
            this.show();
            return;
        }
        // А если инициализирован, то просто показываем последний пикер.
        else {
            this._endPicker.show();
        }
        
    }


    initFields() {
        console.log('Initializing pickers');

        // DC Это кривость необходима, т.к. pikaday не работает с локальным DOM, 
        // а ломится в window, ну а там стилей, естестественно никаких нет.
        // this._lazyLoadCss('/bower_components/pikaday/css/theme.css');

        this._startPicker = new Pikaday({
            field: this.$.startField,
            position: "top left",
            container: this.$.startContainer,
            // Отвязываем пикеры от событий, т.к. иначе они будут сами пытаться себя открыть и тут же закрыть.
            // Выглядит это как мигание календаря. Вроде бы по документации и событие то же самое: focus, но хз как они его обрабатывают.
            bound: false,
            trigger: null,


            minDate: new Date(),
            maxDate: new Date(2020, 12, 31),
            onSelect: () => {
                console.log('start selected');
                let val = this._startPicker.getDate();
                this.startDate = val;
                
                // Это, очередной хак. Т.к. в нашей конфигурации pikaday 
                // не ставит фокус в тестовое поле и там остается пояснительный текст.
                this._mdcStartField.foundation_.inputFocusHandler_();
                
                // this.$.startField.value = val;
                //this.startDate = this._startPicker.getDate();
                this._startPicker.setStartRange(val);
                this._endPicker.setStartRange(val);
                this._endPicker.setMinDate(val);
               
                this._endPicker.show();
                this._startPicker.hide();
            }
        });

        this._endPicker = new Pikaday({
            field: this.$.endField,
            trigger: null,
            bound: false,
            container: this.$.endContainer,
            minDate: new Date(),
            maxDate: new Date(2020, 12, 31),
            onSelect: () => {
                let val = this._endPicker.getDate();
                this.endDate = val;
                
                this._mdcEndField.foundation_.inputFocusHandler_();
                
                this._startPicker.setEndRange(val);
                this._startPicker.setMaxDate(val);
                this._endPicker.setEndRange(val);

                this._endPicker.hide();
            }
        });
        // Вот это тоже своеобразный хак, т.к. пикер при инициализации в соответствии с заданными параметрами
        // сразу же себя открывает.
        this._startPicker.hide();
        this._endPicker.hide();

    }

    // Подгружает css, чтобы pikaday нормально отображался.
    // _lazyLoadCss(href) {
        
    //     var ss = window.document.createElement('link'),
    //     head = window.document.getElementsByTagName('head')[0];

    //     ss.rel = 'stylesheet';
    //     ss.href = href;

    //     // temporarily, set media to something non-matching to ensure it'll
    //     // fetch without blocking render
    //     // ss.media = 'only x';

    //     head.appendChild(ss);

    //     // setTimeout( function(){
    //     //     // set media back to `all` so that the stylesheet applies once it loads
    //     //     ss.media = 'all';
    //     // },0);        
    // }

}

customElements.define(CatPikaday.is, CatPikaday);

