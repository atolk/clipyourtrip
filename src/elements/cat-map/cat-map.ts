/// <reference types="googlemaps" />

// Декларируем для TS поле с глобальным колбэком
interface Window {
    catMapInitCallback?: ()=>void;
}

class CatMap extends Polymer.Element {
    static get is() { return 'cat-map'; }

    $: {
        map: HTMLDivElement
    }

    static get properties() {
        return {
            googleMap: {
                type: Object,
                notify: true,
            },

            googleMapApiKey: String,
            minLocalZoom: Number,

            countryAlpha2: {
                type: String,
                observer: '_countryChanged',
            },


            // границы вьюпорта
            bounds: {
                type: Object,
                notify: true,
            },
            
            mapComponents: {
                type: Array,
            },
            mapMode: {
                type: String,
                notify: true,
            },
            markers: {
                type: Array,
            },
        };
    } // properties

    static get observers()  { return [
        '_markersChanged(markers, googleMap)'
    ]}
    
    connectedCallback() {
        super.connectedCallback();
        this._initGoogleApi();
    }

    // 4TS
    // Нативный объект гугло-карт
    public googleMap: google.maps.Map;
    /**
     * Минимальное значение зума (чем больше, тем ближе) при котором 
     */
    public minLocalZoom: number;
    private _clusterer: MarkerClusterer;

    /**
     * timer keeper for bound event 
     */
    private _boundTimer: number;
    private _currentLayer: google.maps.KmlLayer;
    private _mapPromise: Promise<google.maps.Map>;
    private googleMapApiKey: string;
    private _markers: google.maps.Marker[];
    private _persMarkers: Utils.GoogleMap.Marker[] = [];

    private _lastMapMode: 'global' | 'local';
    
    /**
     * Возвращает обещание инициализированной карты
     */
    ensureInit(): Promise<google.maps.Map> {
        return this._mapPromise;
    }
       
    // Изменение выбранной страны
    _countryChanged(newVal: string) {

        // Удивительно, но урлы регистрозависимые
        let code = newVal.toUpperCase();

        if (this._lastMapMode == 'global') {
        /* Чистим карту */
            this._clear();
            console.log('set layer');
            this._currentLayer = new google.maps.KmlLayer({
                    url: `https://clipyourtrip.ru/kml/${code}.kml`,
                    // preserveViewport: true,
                    map: this.googleMap,
            });
        }
    }

    _markersChanged(newVal: google.maps.Marker[], map: google.maps.Map) {
        if(!(newVal && map)) return;
        
        if (!this._clusterer) {
            this._clusterer = new MarkerClusterer(this.googleMap,null, {
                maxZoom: 15,
                imagePath: 'images/clusters/m'
            });
        }

        console.log('cat-map. markers changed');
        this._clusterer.clearMarkers();

        // если массив маркеров пустой, то давай, до свидания.
        

        this._clusterer.addMarkers(newVal);

    }

    /**
     * Очищает карту от всех маркеров
     */
    _clear() {
        if (this._currentLayer != null) {
            this._currentLayer.setMap(null);
            this._currentLayer = null;
        }
        if (this._markers != null) {
            this._markers.map((m) => m.setMap(null));
            this._markers = null;
        }
    }
  
    _initGoogleApi() {
        this._mapPromise = new Promise<google.maps.Map>((resolve, reject) => {
            // Устанавливаем глобальную функцию, которая будет вызвана после инциаилизации api
            // http://stackoverflow.com/questions/36218205/use-a-callback-in-a-javascript-class
            this._initGoogleMap.bind(this);
            window.catMapInitCallback = () => { this._initGoogleMap(resolve, reject); };
            
            // Загружаем скрипт
            let s = document.createElement('script');
            s.src = `https://maps.googleapis.com/maps/api/js?&libraries=places&key=${this.googleMapApiKey}&callback=catMapInitCallback`;
            s.type = 'text/javascript';
            s.setAttribute('defer', 'defer');
            s.setAttribute('async', 'async');

            window.document.getElementsByTagName('head')[0].appendChild(s);
        });
    }

    _initGoogleMap(resolve: (value?: google.maps.Map) => void, 
                   reject: (reason?: any) => void) {
        try {
            let defaultMapState: google.maps.MapOptions = 
            {
                center: {
                    lat: 29.14514632,
                    lng: 62.44979731
                },
                minZoom: 3,
                zoom: 3,
                fullscreenControl: false
            };
            this.googleMap = new google.maps.Map(this.$.map, defaultMapState);
            // Инициализируем режим карты начальным значением
            this._lastMapMode = this.googleMap.getZoom() >= this.minLocalZoom ? 'local' : 'global';
            this._emitEvent('cat-map-mode-changed', {mode: this._lastMapMode});
            // Подписываемся на события idle, чтобы отслеживать изменение зума и ставить ему в соответствии режим карты
            google.maps.event.addListener(this.googleMap, 'bounds_changed', this._checkMapMode.bind(this));
            google.maps.event.addListener(this.googleMap, 'bounds_changed', this._emitBoundsChangedEvent.bind(this));

            resolve(this.googleMap);
        }
        catch (e){
            reject(e);
        }
    }

    _checkMapMode() {
        let zoom = this.googleMap.getZoom();
        // Если текущий зум больше минимально заданного для определения локального режима карты (чем больше зум, тем ближе)
        // и режим карты был GLOBAL, то 
        if (zoom - this.minLocalZoom >= 0 && this._lastMapMode == 'global') {
            this._lastMapMode = 'local';
            this._emitEvent('cat-map-mode-changed', {mode: 'local'});
        }
        if (zoom - this.minLocalZoom < 0 && this._lastMapMode == 'local') {
            this._lastMapMode = 'global';
            this._emitEvent('cat-map-mode-changed', {mode: 'global'});
        }
    }

    _emitBoundsChangedEvent() {
        clearTimeout(this._boundTimer);

        this._boundTimer = setTimeout(() => 
            {
                this._emitEvent('cat-map-bounds-changed', {bounds: this.googleMap.getBounds()});
            }, 1000);
    }

    /**
     * Отправляет событие
     * @param name 
     * @param detail 
     */
    _emitEvent(name: string, detail: any): void {
        let event = new CustomEvent(name, { bubbles: true, detail: detail});
        this.dispatchEvent(event);
    }


    /**
     * Добавляет маркеры в массив для особого режима отоборажения
     */
    addPersistentMarkers(markers: Utils.GoogleMap.Marker[], clearAll: boolean = false): void {
        // Чистим
        if (clearAll && this._persMarkers) {
            this._persMarkers.forEach(m => m.setMap(null));
            this._persMarkers = [];
        }

        // Добавляем маркеры в массив
        markers.forEach(m => {
            m.setMap(this.googleMap);
            this._persMarkers.push(m);
        });
    }

    /**
     * Удаляет маркер из массива 
     */
    removePersistentMarker(uid: string) {
        let m = this._persMarkers.find(m => m.uid === uid);
        if (m) {
            m.setMap(null);
            let i = this._persMarkers.indexOf(m);
            this._persMarkers.splice(i, 1);
        }
    }

    /**
     * Центрирует карту относительно относительно маркера
     */
    focusOnMarker(uid: string) {
        let m = this._persMarkers.find(m => m.uid === uid);
        if (m) {
            // зумимся и смещаемся
            this.googleMap.panTo(m.getPosition());
            this.googleMap.setZoom(14);
        }
    }
    
}

customElements.define(CatMap.is, CatMap);
