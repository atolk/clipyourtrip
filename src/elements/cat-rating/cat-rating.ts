class CatRating extends Polymer.Element {
    static get is() { return 'cat-rating'; }

    // 4TS
    ['disabled']: boolean;
    ['rating']: number;
    ['max']: number;

    static get properties() { return {
        /**
         * Rating value.
         */
        rating: {
            type: Number,
            notify: true,
            value: 0,
        },
        /**
         * Rating maximum value - this will determine amount of 'stars'.
         */
        max: {
            type: Number,
            value: 5
        },
        /**
         * Disable it for read only mode.
         */
        disabled: {
            type: Boolean,
            value: false,
            reflectToAttribute: true
        },
        _stars: {
            type: Array,
            notify:true,
            computed: '_computeStars(max,rating)'
        }
        
    }} // properties
    
    _computeStars(max: number, rating: number) {
        var result = Array.apply(null, Array(max)).map(Boolean.prototype.valueOf, false);
        for (var i = 0; i < Math.round(rating); i++) {
            result[i] = true;
        }
        return result;
    }

    _updateRating(e: any) {
        if (this.disabled) return;
        this.rating = Number(e.target.dataset['index']) + 1;
    }
}

customElements.define(CatRating.is, CatRating);