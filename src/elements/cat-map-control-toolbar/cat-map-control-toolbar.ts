class CatMapControlToolbar extends Polymer.Element {
    static get is() { return 'cat-map-control-toolbar';}

    $: {
        searchCheckbox: HTMLInputElement,
    }

    static get properties() { return {
    }} // properties

    _btnSaveTapped() {
        console.log('save');
        this.dispatchEvent(new CustomEvent('cat-save', {bubbles: true}));
    }

    _btnCreateTapped() {
        this.dispatchEvent(new CustomEvent('cat-create', {bubbles: true}));
    }

    _btnDoneALlTapped(){
        this.dispatchEvent(new CustomEvent('cat-done', {bubbles: true}));
    }

    _searchModeChanged(e: Event) {
        let val = this.$.searchCheckbox.checked;
        this.dispatchEvent(new CustomEvent('cat-search-enabled', {bubbles: true, detail: {enabled: val}}));
        console.log(val);
    }

}

customElements.define(CatMapControlToolbar.is, CatMapControlToolbar);

