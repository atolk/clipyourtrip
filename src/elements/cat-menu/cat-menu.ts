declare var MaterialMenu: any;

class CatMenu extends Polymer.Element {
    static get is() { return 'cat-menu'; }

    $: {
        btnaction: HTMLElement,
    }

    static get properties() { return {
        icon: String,
        items: {
            type: Array,
            notify: true,
            observer: '_init'
        }
        
    }} // properties

    // 4TS
    items: Array<{displayName: string, id: string}>;
    icon: String;

    connectedCallback() {
        super.connectedCallback();
        
        if (this.items) this._init();
    }

    private _init(){
        // Если массив пустой, то уходим. Какое уж тут меню.
        if (! (this.items && this.items.length)) return;
        
        // Если меню уже стоит, то удаляем его
        this._removeMenu(this.$.btnaction.parentElement);
        
        let menuEl = this._generateMenu(this.items);
        
        this.$.btnaction.parentElement.appendChild(menuEl);
        let menu = new MaterialMenu(menuEl, this);
        
    }

    // Удаляет дочерние меню, если такие есть.
    private _removeMenu(parent: HTMLElement) {
        let menus = parent.getElementsByClassName('mdl-menu__container');
        for(let i = 0; i < menus.length; i++) {
            parent.removeChild(menus[i]);
        }
    }

    private _generateMenu(items: {displayName: string, id: string}[]): HTMLUListElement {
        let menu = document.createElement('ul');
        menu.classList.add(...['mdl-menu','mdl-menu--bottom-right','mdl-js-menu','mdl-js-ripple-effect']);
        menu.setAttribute('for', 'btnaction');

        let self = this;

        let menuItems = items.forEach(item => {
            let menuItem = document.createElement('li');
            menuItem.classList.add('mdl-menu__item');
            menuItem.innerText = item.displayName;
            menuItem.addEventListener('click', function(e) { 
                    let actionEvent = new CustomEvent('cat-menu-action', {detail: item.id, bubbles: true });
                    self.dispatchEvent(actionEvent);
            }, false );
            menu.appendChild(menuItem);
        });
        
        return menu;
    }

}

customElements.define(CatMenu.is, CatMenu);