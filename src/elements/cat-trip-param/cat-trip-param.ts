// 4TS Этот тип у нас подргружается динамически
declare var mdc: any;


class CatTripParam extends Polymer.Element {
    static get is() { return 'cat-trip-param'; }

    $: {
        modalDialog: HTMLElement,
        tripName: HTMLInputElement,
        tripDescription: HTMLInputElement,
        startPoint: HTMLInputElement,
        ac: HTMLInputElement,
        cc: HTMLInputElement,
        pikaday: CatPikaday
    }

    static get properties() { return {

        trip: {
            type: Object,
            notify: true
        },

        tripInfoString: {
            type: String,
            notify: true,
        }
    }} // properties

    static get observers() {return [
        '_updateTripInfoString(trip.*)'
    ]}

    //4TS
    trip: CatBase.Trip;
    tripInfoString: String; 
    _mdcDialog: any;
    

    private _updateTripInfoString(): void {
        let daysStr = this._getDays(this.trip.startDate, this.trip.endDate);
        let strFormat = `Отправление: ${this.trip.startPoint.displayName}, ${this.trip.adultsCount} взрослых, ${this.trip.childrenCount} реб., ${daysStr}`;
        this.tripInfoString =  strFormat;
    }


    private _getDays(startDate: Date, endDate: Date) {
        let nCount = dateFns.differenceInCalendarDays(endDate, startDate);
        return `${dateFns.format(startDate, 'DD.MM.YY, ddd')} на - ${nCount + 1} дн. (${nCount} ноч.)`;
    }

    // Кликнули на редактирование данных
    private _editTapped() {

        this._showDialog();
    }

    // Показывает диалог
    private _showDialog() {
        // Если диалог не инициализирован, то инициализируем его.
        if (!this._mdcDialog) {
            this._mdcDialog = new mdc.dialog.MDCDialog(this.$.modalDialog);
            this._mdcDialog.listen('MDCDialog:accept', () => {
                console.log("OK");
            });
            this._mdcDialog.listen('MDCDialog:cancel', () => {
                console.log("CANCEL");
            });
        }
        this._mdcDialog.show();

    }

    // Инициализирует mdc форму диалога
    private _createMdcDialog(el: HTMLElement): any {
        let dialog;
        // 1. Удостоверимся, что скрип уже загружен, а если нет, то загрузим его.
        // http://stackoverflow.com/questions/14521108/dynamically-load-js-inside-js
        if (typeof mdc === 'undefined') {
            let script = document.createElement('script');
            document.body.appendChild(script);
            script.onload = () => {
                console.log('script loaded');
                this._mdcDialog = new mdc.dialog.MDCDialog(el);
                this._mdcDialog.show();
            };
            script.src = '/node_modules/@material/dialog/dist/mdc.dialog.js';
        }
        else {
            dialog = new mdc.dialog.MDCDialog(el);
        }
        return dialog;

    }

    _okTapped() {
        let tripParam = {
            name:          this.$.tripName.value,
            description:   this.$.tripDescription.value,
            startPoint:    this.$.startPoint.value,
            startDate:     this.$.pikaday.startDate,
            endDate:       this.$.pikaday.endDate,
            adultsCount:   this.$.ac.value,
            childrenCount: this.$.cc.value
        };

        let event = new CustomEvent('cat-param', {bubbles: true, detail: tripParam});
        this.dispatchEvent(event);
        this._mdcDialog.close();
        console.log("OK");
    }

    _cancelTapped() {
        // Перезапиваем исходные значения.
        let trip = this.trip;
        this.set('trip', trip);
        this._mdcDialog.close();
        console.log("CANCEL");
    }


    // https://github.com/PolymerElements/paper-dialog/issues/7
    // _patchOverlay(e) {
        
    //     if (e.target.withBackdrop) {
    //         e.target.parentNode.insertBefore(e.target.backdropElement, e.target);
    //     }
    // }

}

customElements.define(CatTripParam.is, CatTripParam);
