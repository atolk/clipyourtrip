class CatSnackbar extends Polymer.Element {
    static get is() { return 'cat-snackbar';}

    // 4TS
    private _snackbar:any;

    /**
     * Showing a message and action
     */
    show(options: {
            message: string, 
            timeout?: number,
            actionHandler?: () => void,
            actionText?: string,
            multiline?: boolean,
            actionOnBottom?: boolean 
        }): void {
        this._getSnackbarInstance().show(options);
    } // show

    private _getSnackbarInstance(): any {
        if (!this._snackbar) {
            let el = this.shadowRoot.querySelector('.mdc-snackbar');
            this._snackbar = new mdc.snackbar.MDCSnackbar.attachTo(el);
        }

        return this._snackbar;
    }

}

customElements.define(CatSnackbar.is, CatSnackbar);

