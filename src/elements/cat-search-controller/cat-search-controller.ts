class CatSearchController extends Polymer.Element {
    static get is() { return 'cat-search-controller'; }

    // 4TS
    public searchCriterias: CatSearch.Criterias = new CatSearch.Criterias();

    static get properties() { return {
        searchCriterias: {
            type: Object,
            notify: true,
        },
    }} // properties

    removeCriteria(criteria: CatSearch.Criteria): CatSearch.Criteria {
        // Если удаляем таг страны, то удаляем нахрен все критерии поиска. ???
        if (criteria.type === CatSearch.CriteriaType.COUNTRY) {
            this.splice('searchCriterias', 0, this.searchCriterias.length);
            return;
        }
        // Или просто удаляем один таг
        let tagIndex = this.searchCriterias.findIndex(el => el === criteria);
        this.splice('searchCriterias', tagIndex, 1);
    }

    pushCriteria(criteria: CatSearch.Criteria) {
        this.push('searchCriterias', criteria);
        // this.notifyPath('searchCriterias');
    }
}

customElements.define(CatSearchController.is, CatSearchController);