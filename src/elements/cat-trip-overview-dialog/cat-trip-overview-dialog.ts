class CatTripOverviewDialog extends Polymer.Element {
    static get is() { return 'cat-trip-overview-dialog'; }

    static get properties() { return {
        trip: Object,
    }} // properties

    // 4TS
    $: {
        dlgTripOverview: HTMLElement;
    }
    public trip: CatBase.Trip;
    private _mdcDlg: any;

    connectedCallback() {
        super.connectedCallback();
        this._mdcDlg = new mdc.dialog.MDCDialog(this.$.dlgTripOverview);
    }
    
    show() {
        this._mdcDlg.show();
    }

    _closeDialog() {        
        this._mdcDlg.close();
    }

    _getMetaString(trip: CatBase.Trip): string {
        return trip.getMetaString();
    }
  
}

customElements.define(CatTripOverviewDialog.is, CatTripOverviewDialog);