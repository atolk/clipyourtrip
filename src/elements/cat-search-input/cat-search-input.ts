class CatSearchInput extends Polymer.Element {
    static get is() { return 'cat-search-input'; }

    static get properties() {return {
        tags: Array,
        suggestions: Array,
    }}
   
}

customElements.define(CatSearchInput.is, CatSearchInput);