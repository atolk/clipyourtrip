class CatTripOverview extends Polymer.Element {
    static get is() { return 'cat-trip-overview'; }

    // 4TS
    public trip: CatBase.Trip;
    private _dateFormat: String = 'dd,DD.MM';

    static get properties() { return {
        trip: Object
    }} // properties

    _getTripDaysString(startDate: Date, endDate: Date): String {
        
        let nightsCount: number = dateFns.differenceInCalendarDays(endDate, startDate);
        let daysCount: number   = nightsCount + 1;

        let str = `${dateFns.format(startDate, this._dateFormat)} - ${dateFns.format(endDate, this._dateFormat)} ${daysCount} дн. ( ${nightsCount} ноч.)`;
        return str;
    }

    _getDayString(startDate: Date, index: number): String {
        let day = dateFns.addDays(startDate, index);
        let str = `${dateFns.format(day, this._dateFormat)}`;
        return str;
    }

    _getDayNum(index: number): number {
        return index + 1;
    }

    _isEmpty(item: Array<CatBase.Activity>): boolean {
        return item.length == 0;
    }

}

customElements.define(CatTripOverview.is, CatTripOverview);