class CatTagInput extends Polymer.Element {
    $: {
        catSearch: HTMLInputElement,
    }

    static get is() { return 'cat-tag-input';}

    static get properties() { return {

        // Массив состоит из
        tags: Array,

        options: {
            type: Object,
            value: () => {},
        },
    }} // properties

    // 4TS
    tags: CatSearch.Criterias;
    _mdcSearchField: any;
    timer: number;

    connectedCallback() {
        super.connectedCallback();
    }

    static get observers() { return [
        '_tagsChanged(tags.*)'
    ]} // observers

    _closeTapped(e: any) {
        let model = e.model;

        let event = new CustomEvent('cat-tag-remove', {detail: model.item, bubbles: true });
        this.dispatchEvent(event);
    }

    // Нам нужно отслеживать отдельное событие нажатия кнопки чтобы правильно обработать ENTER
    _fieldKeydown(e: KeyboardEvent) {
        let val = this.$.catSearch.value;
        if (!val) return;

        if (e.key === "Enter") {
            this._generateSearchFieldEvent('cat-field-entered', val);
        }
    }

    _fieldChanged(e: Event) {
        let val = this.$.catSearch.value;
        // Если поле чистое, то не генерируем никаких событий
        if (! val) return;
        this._generateSearchFieldEvent('cat-field-changed', val);
    }

    _generateSearchFieldEvent(eventName: string, val: string): void {
        // Устанавливаем таймер и генерируем событие изменений не раньше чем через сек
        // после окончания ввода.
        clearTimeout(this.timer);

        this.timer = setTimeout(() => {
            let e = new CustomEvent(eventName, {
                bubbles: true,
                detail: val,
            });
            this.dispatchEvent(e);
        }, 1000);
    }

    _tagsChanged() {
        /* Кто-то что-то поменял, значит надо очистить поле от поисковых букв. 
           Все. Цель достигнута. Таг найден и выбран. Типа ура.*/
           this.$.catSearch.value = '';
    }
}

customElements.define(CatTagInput.is, CatTagInput);

