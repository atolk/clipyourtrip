class CatSuggestionList extends Polymer.Element {
    static get is() { return 'cat-suggestion-list';}

    $: {
        list: any;
    }

    static get properties() { return {
      /**
         * Массив предложений, составленный по результатам поиска в иточниках поиска.
         */
        suggestions: {
            type: Array,
            notify: true, 
            value: ():CatSearch.Criteria[] => [],
        },

        selectedIndex: {
            type: String,
            notify: true,
            observer: '_selectedIndexChanged',
        },

        selectedSuggestion: { 
            type: Object,
            notify: true,
            computed: '_getSuggestionItem(selectedIndex)'
        },
    }} // properties

    // 4TS
    suggestions: CatSearch.Criterias;

    _selectedIndexChanged(newVal: number, oldVal: number) {
        
    }

    _selected(el: any) {
        var i = this.$.list;
    }
    
    _getSuggestionItem(index: number) {
        if (index == -1) return;
        let suggest = this.suggestions[index];
        this.$.list.selected = -1;
        return suggest;
    }

    _isCountry(val: CatSearch.CriteriaType) {
        return val == CatSearch.CriteriaType.COUNTRY;
    }

    _isPoi(val: CatSearch.CriteriaType) {
        return false;
    }

    _isPlaceTag(val: CatSearch.CriteriaType) {
        return false;
        // return val == CatSearch.CriteriaType.PLACE_TAG;
    }

    _isPlaceType(val: CatSearch.CriteriaType) {
        return val == CatSearch.CriteriaType.PLACE_TYPE;
    }
}

customElements.define(CatSuggestionList.is, CatSuggestionList);

