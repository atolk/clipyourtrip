class CatTripViewListItem extends Polymer.Element {
    static get is() { return 'cat-trip-view-list-item'; }  

    static get properties() { return {
        data: Object,
        dayNightString: {
            type: String,
            notify: true,
            computed: '_getDayNightString(data)'
        },

        btnText: String,
    }}

    _getDayNightString(data:CatBase.Trip) {
        return `дн(нч): ${data.getDaysCount()} (${data.getNightsCount()})`;
    }

    // 4TS
    data: CatBase.Trip; 
}

customElements.define(CatTripViewListItem.is, CatTripViewListItem);