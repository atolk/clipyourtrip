class CatTripController extends Polymer.Element {
    static get is() { return 'cat-trip-controller'; }

    // 4TS
    public trip: CatBase.Trip;
    protected tripProvider: CatDataProvider.TripDataProvider;
    notifyPath: any;

    static get properties() { return {
        trip: {
            type: Object,
            notify: true,
        },
        tripProvider: {
            type: Object,
        }
    }} // properties

    updateTrip(params: any):CatBase.Trip { 
        let newParams = params;
        if (params.startDate || params.endDate)
            newParams.days = this._configureTripDays(newParams.startDate || this.trip.startDate, 
                                                    newParams.endDate   || this.trip.endDate, 
                                                    newParams.days || this.trip.days);
        
        // Перезаписываем только те поля, которые у нас есть.
        this._updateField(this.trip, params);
        this._saveData();
        this._notifyPathWithParams(params);
        // Если произошли какие-либо изменения в датах, то апдейдим массив активностей
        return this.trip;
    }

    /**
     * Уведомляет polymer об изменениях объекта
     * @param params 
     */
    _notifyPathWithParams(params: any) {
        let pathsToNotify = Object.keys(params);

        pathsToNotify.forEach(path => {
            this.notifyPath(`trip.${path}`);
        });

        if (params.startDate || params.endDate) {
            this.notifyPath('trip.days');
        }
    }

    _updateField(obj: any, params: any) {
        Object.keys(obj).forEach(key => 
            {
                // В нашем случае часть свойств может быть равно 0 и это порождает баги,
                // т.к. 0 == false. т.е. вот так вот проверять наличие у объекта свойства if (obj[key]) 
                // в случае значения такого свойства равного нулю дает не верный с т.з. логики приложения результат.
                obj[key] = params.hasOwnProperty(key) ? params[key] : obj[key]
            });
        return obj;
    }

    /** "ChIJE0aku_ib2UYRi2k7kd1EaIs - Толкачевичи"
     * Добавляет активность к перечню активностей дня
     * @param dayIndex индекс дня путешествия (1..n)
     * @param activity Активность для добавления
     */
    addPoi(dayIndex: number, activity: CatBase.Activity): CatBase.Activity {
        this.push(`trip.days.${dayIndex}`, activity);
        this._saveData();

        // Информируем полимер об обновлении массива.
        // https://github.com/Polymer/polymer/issues/3377
        // this.notifySplices(`trip.days.${dayIndex}`, [{
        //     index: dayIndex,
        //     removed: [],
        //     addedCount: 1,
        //     object: this.trip.days[dayIndex],
        //     type: 'splice'
        // }]);
        return activity;
    }

    /**
     * Удаляет POI из переченя активностей заданного дня
     * @param dayIndex индекс дня
     * @param uid уникальный идентификатор POI
     */
    removePoi(dayIndex: number, uid: string): Array<CatBase.Activity> {
        if (dayIndex >= this.trip.days.length) {
            console.warn('Wrong day index. Ignoring...')
            return;
        }
        let index = this.trip.days[dayIndex].findIndex(el => el.uid === uid);
        let removed = this.splice(`trip.days.${dayIndex}`, index, 1);
        this._saveData();

        // this.notifySplices(`trip.days.${dayIndex}`, [{
        //     index: dayIndex,
        //     removed: removed,
        //     addedCount: 0,
        //     object: this.trip.days[dayIndex],
        //     type: 'splice'
        // }]);
        return removed;
    }

    _saveData() {
        Utils.LocalStorage.saveTrip(this.trip);
    }

    _getDefaultMeta(): CatBase.ITripMeta {
        return {
            name : 'Новое путешествие',
            description: '',
            img: '',
            adultsCount : 2,
            childrenCount : 0,
            startPoint : new CatBase.Country(),
            startDate: new Date(),
            endDate: dateFns.addDays(new Date, 7),
            country: new CatBase.Country()
        };
    }

    /**
     * Конфигурирует массив дней (расширяет или сокращает)
     * @param startDate 
     * @param endDate 
     * @param days 
     */
    _configureTripDays(startDate: Date, endDate: Date, days?:Array<CatBase.Activity>[]) {
        // Инициализируем массивы активностей
        let diff:number = dateFns.differenceInCalendarDays(endDate, startDate);
        if (diff <= 0 // || // даты перепутаны
            // dateFns.differenceInCalendarDays(startDate, new Date()) < 0  || // старт в прошлом
            // dateFns.differenceInCalendarDays(endDate, new Date()) < 0  // финиш в прошлом
            )
            { throw new Error("Wrong dates"); }
        
        // ДОбавляем единицу, т.к. действительное количество элементов в массиве не равно 
        // количеству дней между датами.
        diff += 1;

        let retVal: Array<CatBase.Activity>[];
        retVal = days ? days.slice(0) : [];

        // бывает (thnx 2 FIREBASE), что какие-то элементы массива отстуствуют. Это не порядок.
        // Инициализируем их пустыми массивами.
        for (let index = 0; index < retVal.length; index++) {
            if (!retVal[index]) {
                retVal[index] = new Array<CatBase.Activity>(); 
            }
        }

        // Если массив пустой, то просто инициализируем его пустыми массивами
        if (retVal.length == 0)
            retVal = new Array(diff).fill({}).map(() => new Array<CatBase.Activity>());
        
        // Если массив больше, то стрипим его нах.
        if (retVal.length > diff) {
            retVal.length = diff;
        }

        // Если массив меньше, то расширяем диапазон.
        if (retVal.length < diff) 
        {
            let days2add = diff - retVal.length;
            let arr2add = new Array(days2add).fill({}).map(() => new Array<CatBase.Activity>());
            retVal.push(...arr2add);
        }

        return retVal;
    }

    /**
     * Загружает путешествие с uid
     */
    loadTrip(uid: string): Promise<CatBase.Trip> {
        let pr = 
            this.tripProvider.getUserTrip(uid)
            .then(pureTrip => this.updateTrip(pureTrip));
        return pr;
    }

    /**
     * Сохраняет путешествие
     */
    persistTrip(): Promise<any> {
        return this.tripProvider.pushTrip(this.trip);
    }

    _loadFromLocal(): CatBase.Trip {
        // пробуем загрузить из локального хранилища
        let savedTrip = Utils.LocalStorage.readTrip();
        if (savedTrip) {
                let trip = new CatBase.Trip();
                this._updateField(trip, savedTrip);
                return trip;
        }
        else {
            return null;
        }
    }

    _getFreshTrip(): CatBase.Trip {
        let freshTrip = new CatBase.Trip();
        this._updateField(freshTrip, this._getDefaultMeta());
        freshTrip.days = this._configureTripDays(freshTrip.startDate, freshTrip.endDate);
        return freshTrip;
    }

    createNewTrip() {
        this.trip = this._getFreshTrip();
        this._saveData();
    }

    connectedCallback() {
        super.connectedCallback();
        this.trip = this._loadFromLocal() || this._getFreshTrip();
    }
}

customElements.define(CatTripController.is, CatTripController);