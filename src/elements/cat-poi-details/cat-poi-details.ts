class CatPoiDetails extends Polymer.Element {
    static get is() { return 'cat-poi-details';}

    $: {
        photoSwiper: HTMLElement,
        swiperPagination: HTMLElement,
        swiperButtonNext: HTMLElement,
        swiperButtonPrev: HTMLElement,
    }
    
    // 4TP
    ['addTapped']: ()=>void;
    private _swiper: any;
    public data: google.maps.places.PlaceResult;


    static get properties() { return {
        
        addTapped: Object,
        
         data: {
            type: Object,
            observer: '_dataChanged',
        },

        position: {
            type: String,
            value: '',
        }

    }} // properties

    _dataChanged(val: google.maps.places.PlaceResult) {
        //this.hidden = false;
        this._updatePhotos(val);
        //$(this.shadowRoot.querySelectorAll('.tooltipped')).tooltip();

       //$(this.$.slick).fotorama();
        
    }

    _updatePhotos(val: google.maps.places.PlaceResult) {
        this._getSwiper();
        //console.log(val);
        let slides = [`<div class="swiper-slide" style="background-image:url(/images/img_na.jpg)"></div>`];

        if (val && val.photos) {

            let urls = val.photos.map((photo)=>{
                return photo.getUrl({'maxWidth': 600, 'maxHeight': 600});
            });

            slides = urls.map((url) => {
                return `<div class="swiper-slide" style="background-image:url(${url})"></div>`
            });
        }

        this._getSwiper().removeAllSlides();
        this._getSwiper().appendSlide(slides);
    }

    private _getSwiper() {
        if (!this._swiper) {
            this._swiper = new Swiper(this.$.photoSwiper,{
                // Optional parameters
                pagination: this.$.swiperPagination,
                //slidesPerView: 1,
                //paginationClickable: true,
                // width:600,
                // height:600,
                preloadImage: false,
                lazyLoading: true,
                grabCursor: true,
                spaceBetween: 10,
                keyboardControl: true,
                nextButton: this.$.swiperButtonNext,
                prevButton: this.$.swiperButtonPrev,
                loop: true
                
                // // If we need pagination
                // pagination: '.swiper-pagination',
                
                // // Navigation arrows
                // nextButton: '.swiper-button-next',
                // prevButton: '.swiper-button-prev',
                
                // // And if we need scrollbar
                // scrollbar: '.swiper-scrollbar',
            });
        }
        return this._swiper;
    }

    _stripUrl(url: string): string {
        let match = url.match("https?://.+?/");
        return match ? match[0] : url;
    }

    _closeClicked() {
        let event = new CustomEvent('cat-poi-close', {bubbles: true});
        this.dispatchEvent(event);
    }
    


    /**
     * Запускается при нажатии на кнопку добавить. 
     * Если у элемента проставлен колбэк, то запускает его.
     */
    _addClicked() {
        let event = new CustomEvent('cat-poi-add', {bubbles: true});
        this.dispatchEvent(event);
    }
}

customElements.define(CatPoiDetails.is, CatPoiDetails);

