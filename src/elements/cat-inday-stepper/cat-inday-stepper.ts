/// <reference path="../../../bower_components/date-fns/typings.d.ts" />

class CatIndayStepper extends Polymer.Element {
    static get is() { return 'cat-inday-stepper'; }

    // 4TS
    trip: CatBase.Trip;
    $: {
        indayNav: HTMLElement;
    }

    static get properties() { return {
        selected: String,
        trip: Object,
        baseHref: String,
    }} // properties

    static get observers() { return [
        '_tripDaysChanged(trip.days)'
    ]}

    _tripDaysChanged(days: CatBase.Activity[]) {
        if (!days) return;

        this.$.indayNav.style.width = `${100 * days.length}px`;
    }

    _getNavString(index: number) {
        let strDate = this._getFormatedDate(index);
        return `${strDate}`;
    }

    _getDisplayIndex(i: number): number {
        return i + 1;
    }

    _getArrayIndex(i: number): number {
        return i - 1;
    }

    _getFormatedDate(i: number): string {
        let date = dateFns.addDays(this.trip.startDate, i);
        return dateFns.format(date,  'ddd D.MM');
    }

    _devTapped(): void {
        this.push('trip.days.2', 1);
    }
}

customElements.define(CatIndayStepper.is, CatIndayStepper);
