class CatPoiList extends Polymer.Element {
    static get is() { return 'cat-poi-list'; }

    // 4TS
    ['items']: Array<CatBase.Activity>;

    static get properties() { return {
        items: Array,
    }} // properties

    connectedCallback() {
        super.connectedCallback();

        // Инициализируем функци D&D

        // let el = sortable(this.$.container);
        // console.log(el);

        // let el = Sortable.create(this.$.container, {
        //     onEnd: e => console.log(e),
        //     druggable: '.btn'
        // });
        // Sortable.create(this.$.sortme);
        // this.initialize();
    }

    // Кликнули на крестик у POI
    _removeTapped(e: any) {
        this._emitEvent('cat-poi-remove', {poi: e.model.item});
    }

    _poiTapped(e: any): void {
        this._emitEvent('cat-poi-clicked', {poi: e.model.item});
    }

    _clicked(e: Event) {


        // console.log(e);
        // console.log(e.currentTarget);
        // console.log(e.currentTarget.parentElement.getElementsByClassName('fab-origin'));
        // console.log(this);
        // let els = e.currentTarget.parentElement.getElementsByClassName('fab-origin');
        // if (els.length > 0)
        // {
        //     let btn = $(els[0]);
        //     if (btn.hasClass('active')) {
        //         btn.closeFAB();
        //         btn.hide(); // Это хак для отображения при перестакивании только контура 
        //     }
        //     else {
        //         this._closeAllMenu();
        //         btn.show(); // Это хак для отображения при перестакивании только контура
        //         btn.openFAB();
        //     }
        // }
    }

    _closeAllMenu() {
        // let els = this.$.container.querySelectorAll('div > div.fab-origin.active');
        // // Старый добрый for, т.к. NodeList не Array.
        // for(let i = 0; i < els.length; i++)
        // {
        //     let item = els[i];
        //     $(item).closeFAB();
        //     $(item).hide();
        // }
        
    }

    _emitEvent(name: string, data: any): void {
        let event = new CustomEvent(name, {bubbles: true, detail: data});
        this.dispatchEvent(event);
    }
 
}

customElements.define(CatPoiList.is, CatPoiList);
