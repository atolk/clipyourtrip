namespace CatDataProvider {
    /**
     * Отвечает за все провайдеры внешних данных для сервиса
     */
    export class CatDataProviderManager {

        private _mapProvider: MapDataProvider;
        private _tripProvider: TripDataProvider;
        private _searchProvider: SearchDataProvider;

        /**
         * Провайдер операций с объектами путешествий
         * @public
         */
        get tripProvider(): TripDataProvider {
            if (!this._tripProvider) {
                this._tripProvider = new CatProviderTripStorage();
            }
            
            return this._tripProvider;
        }

        /**
         * Провайдер данных google-сервисов
         */
        get mapProvider(): MapDataProvider{
            if (!this._mapProvider)
                this._mapProvider = new CatProviderGoogle();
            return this._mapProvider;
        }

        /**
         * Провайдер данных о стране
         */
        get searchProvider(): SearchDataProvider {
            if (!this._searchProvider)
                this._searchProvider = new CatProviderSearch();
            return this._searchProvider;
        }
    }
}

var CatDataProviderManager: CatDataProvider.CatDataProviderManager = new CatDataProvider.CatDataProviderManager();