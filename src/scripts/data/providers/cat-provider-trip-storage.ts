namespace CatDataProvider {

/**
 * Представление хранилища объектов путушествий, реализующего механизм безопасности
 */
export class CatProviderTripStorage implements TripDataProvider {
    private _fbApp: firebase.app.App;

    constructor() {
        if (!firebase)
            throw new Error('failed to initialize provider with firebase');
        
        this._fbApp = firebase.app();
    }

    /**
     * Возвращает перечень пользовательских путешествий, сохраненных в личном пространстве
     */
    getUserTripsMeta(): Promise<CatBase.Trip[]> {

        let pr = new Promise<CatBase.Trip[]> ((resolve, reject) => {
            
            // Начинаем с получения текущего пользовательского пути к данным.
            this._getUserPath()
                .then(path => {
                    let fullPath = path + '/meta';
                    let ref = this._fbApp.database().ref(fullPath);

                    return ref;
                })
                // теперь запускаем еще один асинхронный запрос, который должен получить данные.
                .then( (ref) => {
                    let snap = ref.once('value'); 
                    // Явно говорим TypeScript какой тип придет.
                    return snap as Promise<firebase.database.DataSnapshot>;
                })
                .then(snap => {
                    let valObj = snap.val();
                    // Это выражением вытекает из того, что данные из firebase к нам приходят в виде плоского объекта,
                    // у которого key - это идентификатор мета-инфа путешествия,
                    // а значение по этому ключу - сама метаинфа.
                    let trips: CatBase.Trip[] = Object.keys(valObj).map(key => valObj[key]);

                    // Контруируем настоящие объекты, с методами и блэкджеком.
                    trips = trips.map(el => Object.assign(new CatBase.Trip(), el));

                    resolve(trips);
                });
        });

        return pr;
    }

    /**
     * Возвращает полный объект выбранного путешествия
     * @param {string} ID путешествия
     */
    getUserTrip(uid: string): Promise<CatBase.Trip> {
        let pr = new Promise<CatBase.Trip>((resolve, reject) => {
            this._getUserPath()
                    .then(path => {
                        let fullPath = path + '/data/' + uid;
                        let ref = this._fbApp.database().ref(fullPath);
                        return ref; })
                    .then((ref) => {
                        let snap = ref.once('value');
                        return snap as Promise<firebase.database.DataSnapshot>;
                    })
                    .then(snap => {
                        let trip: CatBase.Trip = snap.val();
                        // ХАК! Злоебучий firebase не возвращает массив, а возвращает объект с числовыми полями.
                        // И вообще заебал он со своими Array is EVIL статьями.
                        try {
                            let firebaseArrayVision = trip.days;
                            trip.days = [];
                            Object.keys(firebaseArrayVision).forEach(key  => {
                                    let i = parseInt(key);
                                    trip.days[i] = firebaseArrayVision[i]
                                });
                        }
                        catch (e) {
                            reject(e);
                        }
                        resolve(trip);
                    });
        });
    
        return pr;
    }

    /**
     * Возвращает полный объект выбранного путешествия
     * @param {*} ID путешествия
     * @param {*} uid идентификатор пользователя
     */
    getTripDetails(tripId: string) {
        throw new Error('Not implemented');
    } 


    /**
     * Добавляет путешествие в пользовательское пространство, 
     * возвращает uid нового или уже существующего путешствия
     * @param {CatBase.Trip} trip 
     */
    pushTrip(trip: CatBase.Trip): Promise<string> {
        let pr = 
            this._getUserPath()
                .then(path => {

                    console.log(trip);
                    

                    let ref = this._fbApp.database().ref(path);
                    // Если у объекта нет uid, то получаем его!
                    if (!trip.uid) {
                        let key = ref.child('meta').push().key;
                        trip.uid = key;
                        trip.createdDate = new Date();
                    }
                    // Апдейтим поле со временем апдейта
                    trip.lastUpdatedDate = new Date();
                    // готовим апдейт базы
                    let updates: any = {};
                    updates['meta/' + trip.uid] = trip.getMeta();;
                    updates['data/' + trip.uid] = trip;

                    let promise = ref.update(updates);
                    return promise;
                });
        return pr;
    }

    deleteTrip(uid: string) {
        let pr = 
            this._getUserPath()
                .then(path => {
                    let ref = this._fbApp.database().ref(path);
                    let updates = {} as any;
                    updates['meta/' + uid] = null
                    updates['data/' + uid] = null;

                    let promise = ref.update(updates);
                    return promise;
                });
        return pr;
    }

 
    /**
     * Returns promise firebase db ref belongs to current user
     */
    _getUserPath() {
        let pr = this._getAuthUser()
            .then(user => {
                return `users/${user.uid}/trips`;
            });
        return pr;

    }


    /**
     * Returns promise about cuurent user
     * 
     */
    _getAuthUser(): Promise<firebase.User> {
        /* Все дело в том, что если сразу пойти на страничку с персонифицированной информацией,
         * например, в редактор текущего путешествия или список моих путешествий, 
         * то объект auth() может находится еще в стадии инициализации и вернуть null. 
         * Рекомендуем путь получения текущего пользователя - callback. 
         * см. https://firebase.google.com/docs/auth/web/manage-users */
        let pr = new Promise<firebase.User>((resolve, reject) => {
            this._fbApp.auth().onAuthStateChanged(
                (user: firebase.User) => user ? resolve(user) : reject ('Not authenticated')
            )
        });

        return pr;
    }
}
}