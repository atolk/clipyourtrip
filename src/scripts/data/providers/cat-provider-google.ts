namespace CatDataProvider {

export class CatProviderGoogle implements MapDataProvider {

    private _gMap: google.maps.Map;
    private _placesService: google.maps.places.PlacesService;

    init(gMap: google.maps.Map) {
        this._gMap = gMap;
    }

    getDetail(uid: string): Promise<google.maps.places.PlaceResult> {
        let pr = new Promise<google.maps.places.PlaceResult>((resolve, reject) => {
            this.placesService.getDetails({placeId: uid}, (place, status) => {
                // Все ок. Резолвим промис
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    resolve(place);
                }
                // что-то не так, а причин может быть дохера
                else {
                    reject(new Error(`google service failed to copmplete request with status ${status}`));
                }
            });
        });
        return pr;
    }

    getData(criterias: CatSearch.Criterias):Promise<any> {
        if (!this._gMap) throw 'initalize first with google maps';

        let promise = new Promise((resolve, reject) => {
            let keyword: string = criterias.getKeywordAsString();
            let types: string[] = criterias.getPlaceTypes();
            
            // Если не задано ни то, ни другое, то по-умолчанию будем искать жилье
            if (!(keyword || types.length)) {types = ['lodging'];}

            let request = {
                        bounds: this._gMap.getBounds(),
                        keyword: keyword,
                        types: types };
            try {
                this.placesService.radarSearch(request, (result, status) => {
                    if (status == google.maps.places.PlacesServiceStatus.OK || 
                        status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS) {
                        resolve(result);
                    }
                    else {
                        reject(status);
                    }
                });
            }
            catch(err) {
                reject(err);
            }
        });
        return promise;
    }

    /**
     * Детализированная информация об объекте
     * @param {String} param0 
     */
    getDetails( placeId: string ) {
        if (!this._gMap) throw 'initalize first with google maps';
        let promise = new Promise((resolve, reject) => {

            this.placesService.getDetails({placeId}, (result, status) => {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                        resolve(result);
                    }
                    else {
                        reject(status);
                    }
            });
        });
        return promise;
    }



    private get placesService():google.maps.places.PlacesService {
        if (!this._placesService)
            this._placesService = new google.maps.places.PlacesService(this._gMap);
        return this._placesService;
    }
}
}