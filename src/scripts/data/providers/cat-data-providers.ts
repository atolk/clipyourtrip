/// <reference path="./../../base/cat-trip.ts" />
/// <reference path="./../../search/cat-search.ts" />


namespace CatDataProvider {

    /**
     * Интерфейс провайдера мета-данных (собственных публичных данных) пиложения
     */
    export interface SearchDataProvider {
        /**
         * Список типов мест
         */
        getPlaceTypes(crtierias: string): Promise<CatSearch.Criteria[]>;
        /**
         * Возвращает список стран в форме поисковых критериев, соответствующих заданным поисковым критериям :)
         */
        getCountryList(crtierias: string): Promise<CatSearch.Criteria[]>;
    }

    /**
     * Интерфейс картографических данных
     */
    export interface MapDataProvider {
        
        getData(criterias: CatSearch.Criterias): Promise<google.maps.places.PlaceResult[]>;
        /**
         * Возвращает обещание о подробной информации о конкретном месте
         */
        getDetail(uid: string): Promise<google.maps.places.PlaceResult>;
        init(data: google.maps.Map): void;
    }

    /**
     * Интерфейс хранилища путешествий
     */
    export interface TripDataProvider {
        getUserTripsMeta(): Promise<CatBase.Trip[]>;
        getUserTrip(uid: string): Promise<CatBase.Trip>;
        deleteTrip(uid: string): Promise<string>;
        pushTrip(trip: CatBase.Trip): Promise<string>;
    
    }
}