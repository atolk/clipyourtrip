/// <reference path="../../search/cat-search.ts" />

namespace CatDataProvider {
    export class CatProviderSearch implements SearchDataProvider {
        private _apiBaseUrl = 'https://api.clipyourtrip.ru';
        private _apiCountriesUrl = this._apiBaseUrl + '/countries';
        private _apiPlaceTypesUrl = this._apiBaseUrl + '/place-types';

        /**
         * Кэш объектов URL
         */
        private _apiUrlCache: {[url: string]: URL} = {};


        // private _placeTypesUrl = 'https://1charter.ru/placeTypes.json';
        // private _countriesUrl = 'https://1charter.ru/countries.json';
        // private _cache: any[];


        /**
         * 
         * @param criterias Критерии, которым должны соответствовать типы мест.
         */
        getPlaceTypes(criterias: string): Promise<CatSearch.Criteria[]> {
            if (typeof criterias === "string")
            {
                return this._getSearchCriterias(this._apiPlaceTypesUrl, 
                                            {'name_rus[$search]': criterias},
                                            el => new CatSearch.Criteria(el.value, 
                                                                         CatSearch.CriteriaType.PLACE_TYPE, 
                                                                         el.name_rus));
            }
            else 
                throw new Error('function call with this param types not implemented yet');
        }

        /**
         * Загружает данные и возвращает обещание массива поисковых вариантов
         * @param url 
         * @param Фильтр, который будет использоваться для фильтрации значений
         * @param конструктор новых элементов
         */
        private _getSearchCriterias(url: string, params: {[key: string]:string},
                                valConstructor: (el: any) => CatSearch.Criteria):Promise<CatSearch.Criteria[]> {
            
            return new Promise<CatSearch.Criteria[]>((resolve, reject) => {
                    /* Загружаем данные, если их еще нет */
                    try {
                        this._getJson(url, params)
                            .then(res => {
                                let sc = res.map(valConstructor);
                                resolve(sc);
                            });
                    }
                    catch(e) {
                        reject(e);
                    }
                });

        }
        

        getCountryList(criterias: string): Promise<CatSearch.Criteria[]> {

            return this._getSearchCriterias(this._apiCountriesUrl, 
                                        {'name_rus[$search]': criterias},
                                        el => new CatSearch.Criteria(el.alpha2, 
                                                                     CatSearch.CriteriaType.COUNTRY, 
                                                                     el.name_rus));
        
            
        }

        /**
         * Отдает JSON объект с данными, а если его нет, то загружает и сохраняет в кэше
         * @param url 
         */
        private _getJson(url: string, params: {[key: string]: string}):Promise<any[]> {
            return new Promise<any[]>((resolve, reject) => {
                // https://github.com/github/fetch/issues/256

                let actUrl = this._getApiUrl(url); // any здесь вот по-этому: https://github.com/Microsoft/TypeScript/pull/15262
                
                Object.keys(params).forEach(key => actUrl.searchParams.append(key, params[key]));
                fetch(actUrl.toString())
                //.then(console.log)
                .then(res => res.json() as Promise<any>)
                .then(res => {
                    // хак для TS, который неправильно определяет тип респонса в промисах
                    let response: any = res;
                    if(response) {
                        resolve(response.data); 
                    }
                    else {
                        reject(new Error("Stream data broken"));
                    }
                    });
            }); // promise
        }

        /**
         * Возвращает объект URL для заданного адреса.
         */
        private _getApiUrl(url: string): URL {
            if (!this._apiUrlCache[url]) {
                this._apiUrlCache[url] = new URL(url);
            }
            // Удаляем параметры
            let params = Array.from(this._apiUrlCache[url].searchParams.keys());
            params.forEach(key => this._apiUrlCache[url].searchParams.delete(key));

            return this._apiUrlCache[url];
        }
    }
}