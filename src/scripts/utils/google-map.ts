/// <reference types="googlemaps" />

namespace CatBase {
    export interface Activity {
        /**
         * Конвертирует POI в маркер
         */
        toMarker: (clickHandler?: () => void) => Utils.GoogleMap.Marker;
    }
    Activity.prototype.toMarker = function (clickHandler?: () => void) { 
        let s = this as Activity;
        let m = Utils.GoogleMap.createMarker(s.uid, {
            position: s.latlng,
            icon: { url: s.icon,
                    scaledSize: new google.maps.Size(35, 35) },
        });
        if (clickHandler) {
            google.maps.event.addListener(m, 'click', clickHandler);
        }
        return m;
    };
}

namespace Utils {
    export namespace GoogleMap {

        export type Marker = google.maps.Marker & {uid: string};

        export function createMarker(uid: string, 
                              opts: google.maps.MarkerOptions): Marker{
            let m = new google.maps.Marker(opts);
            return Object.assign(m, {uid});
        }

        // export class Marker extends google.maps.Marker {
        //     constructor(public uid: string, opts?: google.maps.MarkerOptions) {
        //         super(opts);
        //     }
        // }

        /**
         * Конвертируем массив 
         * @param places 
         */
        export function convert2markers(places: google.maps.places.PlaceResult[],
                                        onClick: (place: google.maps.places.PlaceResult)=>void): google.maps.Marker[] {
            let markers = places.map(pl => {
                let m = createMarker(pl.place_id, {
                        position: pl.geometry.location
                    });
                google.maps.event.addListener(m, 'click', () => {onClick(pl)});
                return m;
            });
            return markers;
        }
    }
}