namespace Utils {
    export namespace LocalStorage {

        export function saveTrip(trip: CatBase.Trip) {
            let str = JSON.stringify(trip);
            window.localStorage.setItem('cat-trip', str);
        }

        export function readTrip(): CatBase.Trip {
            let str = window.localStorage.getItem('cat-trip');
            if (!str) return;
            let trip = new CatBase.Trip(JSON.parse(str));
            return trip;
        }

        export function saveMapState(state: any) {
            let str = JSON.stringify(state);
            localStorage.setItem('cat-map-state', str);
        }

        export function readMapState():MapState {
            let str = localStorage.getItem('cat-map-state');
            if (!str) return;
            let mapState = JSON.parse(str);
            return mapState;
        }

        export class MapState {
            center: {
                lat: number,
                lng: number
            }
            zoom: number
        }
    }
}