namespace CatSearch {
    /**
     * Класс представляет собой атомарный критерий поиска.
     */
    export class Criteria {
        /**
         * Создание нового объекта
         * @param {String} value 
         * @param {CatSearch.CriteriaType} type 
         * @param {String} displayName 
         */
        constructor(public value: string, 
                    public type: CriteriaType = CriteriaType.KEYWORD, 
                    public displayName:string = value ) {
        }
    }

    /**
     * Тип поисковой корпускулярии
     */
    export enum CriteriaType {
        COUNTRY = 1,
        PLACE_TYPE,
        KEYWORD
    }

    /**
     * Массив поисковых критериев
     */
    export class Criterias extends Array<Criteria> {
        /**
         * Добавляет критерий поиска в массив и убеждается, что страна в нем одна.
         * @param {CatSearch.Criteria} criteria 
         */
        push(criteria: Criteria): number {
            let i:number = -1;
            if (criteria.type == CriteriaType.COUNTRY) {
                i = super.findIndex(
                    el => el.type == CriteriaType.COUNTRY
                );
            }

            // Если в тэгах еще нет страны,  то просто пушим.
            // а вот если есть - то делаем splice, чтобы все изменения (удаление + добавление) 
            // подхватились за один раз/
            if (i == -1) {
                i = super.push(criteria);
            }
           else {
                this[i] = criteria;
            }               
            return i;
        }
        /**
         * Возвращает строку, содержащую ключевые слова (или их части), разделенные пробелом
         */
        getKeywordAsString(): string {
            let keywords: string = super.filter(el => el.type == CriteriaType.KEYWORD).join(' ');
            return keywords;
        }

        /**
         * Возвращает массив типов мест
         */
        getPlaceTypes():string[] {
            let types = super.filter(el => el.type == CriteriaType.PLACE_TYPE)
                        .map(el => el.value);
            return types;
        }
    }
}