namespace CatBase {

    export class Country {
        displayName: string;
        value: string;

        constructor(displayName:string = '-', value: string = '') {
            this.value = value;
            this.displayName = displayName;
        }
    }
  
    // export interface IActivity {
    export interface Activity {

    }

    type ActivityOptions = Partial<Activity>;

    // }
    /**
     * Активность. POI, отель, бассейн, и пр...
     */
    export class Activity implements Activity {
        public displayName: string = undefined;
        public uid: string = undefined;
        public icon: string = undefined;
        public latlng: {lat: number, lng: number}
        
        constructor(opts: ActivityOptions) {
            if (opts) {
                this._init(opts);
            }
        }

        _init(opts: ActivityOptions) {
            Object.assign(this, opts);
        }
    }

    type TripOptions = Partial<Trip>;

     /**
     * Класс олицетворяющий путешествие
     */
    export class Trip implements ITripMeta{
        constructor(opts?: TripOptions) {
            
            if (opts) this._init(opts);
        }

        _init(opts: TripOptions) {
            Object.assign(this, opts);
            if (opts.days) {
                this.days = opts.days.map(poiList => {
                    return poiList.map(poi => {
                        return new Activity(poi);
                    });
                });
            }
        }

       /**
         * Уникальный идентификатор путешествия
         */
        uid: string = undefined;
        /**
         * Название путешествия
         */
        name: string = undefined;

        /**
         * URL картинки
         */
        img: string = undefined;
        /**
         * Описание путешествия
         */
        description: string = undefined;
        /**
         * Дата отправления
         */
        startDate: Date = undefined;
        /**
         * Дата прибытия
         */
        endDate: Date = undefined;
        /**
         * Дата первичного создания
         * по идее, проставляется один раз при первом сохранении
         */
        createdDate: Date = undefined;

        /**
         * Дата последнего сохранения
         */
        lastUpdatedDate: Date = undefined;

        /**
         * Страна путешествия
         */
        country: Country = undefined;

        // Страна старта
        startPoint: Country = undefined;

        days: Array<Activity[]> = undefined;

        /**
         * 
         */
        adultsCount: number = undefined;
        childrenCount: number = undefined;

        getMeta():ITripMeta {
            return {
                uid: this.uid,
                name: this.name,
                img: this.img,
                description: this.description,
                startDate: this.startDate,
                endDate: this.endDate,
                startPoint: this.startPoint,
                country: this.country,
                adultsCount: this.adultsCount,
                childrenCount: this.childrenCount
            };
        }

        /**
         * Количество дней путешествия
         */
        getDaysCount(): number {
            return dateFns.differenceInCalendarDays(this.endDate, this.startDate) + 1;
        }

        getNightsCount(): number {
            return dateFns.differenceInCalendarDays(this.endDate, this.startDate);
        }

        /**
         * Формирует локализованную строку, описывающее путешествие (нач - кон дн(ноч), взр.+дет)
         */
        getMetaString(): string {
            let strStart = dateFns.format(this.startDate, 'DD.MM');
            let strEnd = dateFns.format(this.endDate, 'DD.MM');
            let val = `${strStart} - ${strEnd}, ${this.getDaysCount()}д. (${this.getNightsCount()}н.)`;

            if (this.adultsCount) {
                val += `, ${this.adultsCount}вз. (${this.childrenCount}дет.)`;
            }
            return val;
        }
        
        // getData(): Array<Activity[]> {
        //     throw new Error('Not implemented');
        // }
    }

    // export function createTrip(opts: Trip): Trip {
    //     if(opts.days) {
    //         let newDays = opts.days.map(poiList => {
    //             return poiList.map(poi => {
                    
    //             });
    //         });
    //     }
    //     let newTrip = new Trip();
    //     Object.assign(trip, params);
    // }

    export interface ITripMeta {
        uid?: string,
        name: string,
        img: string,
        description: string,
        startDate: Date,
        endDate: Date,
        startPoint: Country,
        country: Country,
        adultsCount: number,
        childrenCount: number
    }
 

}