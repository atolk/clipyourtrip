declare var componentHandler: any;

class CatApp extends Polymer.Element {
    static get is() { return 'cat-app'; }

    $: {
        appheader: HTMLElement,
        userMenu: CatMenu,
        auth: any,
        catDrawer: HTMLElement,
        snackbar: CatSnackbar,
    }

    static get properties() { return {

        tripController: Object,
        mapProvider: Object,
        searchProvider: Object,

        isAuthenticated : {
            type: Boolean,
            notify: true,
            value: false,
        },

        user: Object,

        // текущий роутинг
        routeData: Object,
        queryParams: Object,
        // Страница, которая в роутинге, вовсе не обязательно соответствуюет отображаемой вьюхе
        // Так мы реализуем кейс с корнем сайта.
        viewPage: String, 

        // Основная навигация
        _navItems:
        {
            type: Array,
            value: [
                {name: 'browse', label: 'База маршрутов'},
                {name: 'trip',   label: 'Конструктор путешествия'},
                {name: 'help',   label: 'Как это работает'},
                {name: 'about',  label: 'Напишите нам'},
            ],
        },
 
    }} // properties

    static get observers(){ return [
        '_routePageChanged(routeData.page)',
    ]} //observers

    // 4TS
    tripProvider: CatDataProvider.TripDataProvider;
    mapProvider: CatDataProvider.MapDataProvider;
    searchProvider: CatDataProvider.SearchDataProvider;
    viewPage: string;
    
    // левое меню
    _drawer: {open: boolean};

    isAuthenticated: boolean;
    user: firebase.auth.Auth;

    routeData: {page: string};

    /**
     * ХАК! Проверить
     **/
    // resolveUrl: (url: string) => string;

    connectedCallback() {
        
        super.connectedCallback();
        this._checkSWCache();

        this.initialize();
        this._drawer = new mdc.drawer.MDCTemporaryDrawer(this.$.catDrawer);
        // new mdc.tabs.MDCTabBar(this.shadowRoot.querySelector('nav.cat-main-nav'));
        
        // регистрируем колбэк, который будет держать маркер аутентификации в актуальном состоянии
        // и заодно менять перечень доступного меню
        firebase.auth().onAuthStateChanged((user: firebase.auth.UserCredential) => {

           this.set('isAuthenticated', user != null);

           if (this.isAuthenticated) {
               this.$.userMenu.items = [];
               this.$.userMenu.items = [{displayName: 'Выйти', id: 'logout'}];
           }
           else {
               this.$.userMenu.items = [];
               this.$.userMenu.items = [{displayName: 'Войти', id: 'login'}];
           }

        });
    }

    _showDrawer(){
        this._drawer.open = true;
    }
    _closeDrawer(){
        this._drawer.open = false;
    }

    initialize() {
        this.mapProvider = CatDataProviderManager.mapProvider;
        this.searchProvider = CatDataProviderManager.searchProvider;
        this.tripProvider = CatDataProviderManager.tripProvider;
    }

    _routePageChanged(page: string) {
        
        if (page === undefined) return;

        // Если страница "" то задаем вьюху для отображения.
        this.set('viewPage', page || 'main');
        

        // резолвим url реальной страницы
        let catPage: string;
        // Если вдруг страница my-trips, то ее отображает вьюха trip-list
        if(this.viewPage === 'my-trips') {
            catPage = 'cat-trip-list';
        }
        else {
            catPage = `cat-${this.viewPage}`;
        }

        const resolvedPageUrl = this.resolveUrl(`../${catPage}/${catPage}.html`);
        
        // пробуем ипортировать, а если не получилось, то роутим на 404.
        Polymer.importHref(resolvedPageUrl, null, ()=> this.routeData = {page: '404'}, true);
        console.log('Page changed: ', resolvedPageUrl, ' ', catPage);

        // if (!this.$.drawer.persistent) {
        //     this.$.drawer.close();
        // }
    }

    _showPage404() {
        console.warn('page not found');
        //this.page = '404';
    }

    _menuClicked(e: CustomEvent) {
        this.$.auth.provider = "google";
        switch(e.detail) {
            case "login": this.$.auth.signInWithPopup(); break;
            case "logout": this.$.auth.signOut(); break;
        }
    
    }

    _checkSWCache() {
        if (navigator.serviceWorker && navigator.serviceWorker.controller) {
                navigator.serviceWorker.controller.onstatechange = (e: any) => {
                    if (e.target.state === 'redundant') {
                        this.$.snackbar.show({
                            message: "Доступна новая версия. Обновите приложение.",
                            timeout: 1000 * 30,
                            actionText: "ОБНОВИТЬ",
                            actionHandler: () => window.location.reload(true)
                        });
                    }
                };
            }
    }
}

customElements.define(CatApp.is, CatApp);

