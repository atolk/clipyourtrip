interface SearchProviders {
    [mapMode: string]: (criteria: string) => Promise<CatSearch.Criteria[]>;
}

class CatTrip extends Polymer.Element {
    static get is() {return 'cat-trip'; }
    
    $: {
        tripController: CatTripController,
        searchController: CatSearchController,
        snackbar: CatSnackbar,
        tripOverviewDialog: CatTripOverviewDialog,
        mapContainer: HTMLElement,
        poiDetails: CatPoiDetails,
        poiDetailsContainer: HTMLElement,
        suggestList: CatSuggestionList,
        catMap: CatMap,
    }
    
    

    // Хак, чтобы TS успокоился на тему свойств, определенных черех полимер.
    
    // не работаем в коде с ним напрямую TS не должен знать о таком поле.
    // private trip: CatBase.Trip;
    // private searchCriterias: CatSearch.Criterias;
    private pageNum: number;
    private _mapMode: 'global' | 'local';
    private mapProvider: CatDataProvider.MapDataProvider;
    private searchProvider: CatDataProvider.SearchDataProvider;
    private currentDayActivities: Array<CatBase.Activity>;
    private googleMap: google.maps.Map;
    private _searchProviders: SearchProviders = {};
    private _isSearchEnabled: boolean = false;

    private _tripController: CatTripController;
    private _searchController: CatSearchController;

    static get properties() { return {

        mapProvider: Object,
        searchProvider: Object,
        tripProvider: Object,
        

        googleMapApiKey: {
            type: String,
            readonly: true,
            value: 'AIzaSyAVP1B1G-ot9W-xu8-nHz9U5yF0AmCb9mQ'
        },

        /* Этот объект в этом компоненте нам необходим потому, что cat-trip
         так же является и медиатором в отношении своих дочерних компонент, 
         а часть (большая) из них нуждается в инициализации с использованием гугло-карт. (PlacesService)
        */
        googleMap: {
            type: Object,
            notify: true,
            observer: '_initMapEventListeners',
        },
        // Нужен для того, чтобы этот массив подхватывал компонент с отображением активностей
        // внутри убираем dirty checking/
        currentDayActivities: {
            type: Array,
            notify: true,
        },

        /**
         * Содержит результаты поиска со всех источников данных в соответствии с текущими настройками
         */
        suggestions: {
            type: Array,
            notify: true,
            value: ():CatSearch.Criteria[] => [],
        },

        /**
         * Выбранный объект из списка дополнений (подсказок)
         */
        selectedSuggestion: {
            type: Object,
            observer: '_selectedSuggestionChanged'
        },

        /**
         * Детали последнего выбранного объекта
         */
        currentPoi: {
            type: Object,
            notify: true,
        },

        /**
         * Массив элементов поиска
         */
        searchCriterias: Array,

        subroute: Object,
        routeData: Object,
        queryParams: Object,
        pageNum: Number,
        route: Object,

        // Никогда не манипулируем напрямую, только через tripController
        trip: {
            type: Object,
            notify: true
        }, // cat

        markers: {
            type: Array,
            notify: true,
            value: (): google.maps.Marker[] => [],
        },

        components: Array,

        
    }} // properties
    

    connectedCallback() {
        super.connectedCallback();
        this._searchProviders['global'] = search => this.searchProvider.getCountryList(search);
        this._searchProviders['local'] = search => this.searchProvider.getPlaceTypes(search);
    }

    static get observers() {return [
        // TL;DR: dirty cheking
        // см. комментарии в теле почему так, а не вычисляемое свойство.
        '_updateCurrenDaytActivities(trip.days.*, pageNum)',
        '_updatePersistenMarkers(trip.days.*, googleMap)',

        '_routePageChanged(routeData.page)',

        '_queryParamsChanged(queryParams.uid)',
    ]} // observers

    _newTripParams(e: CustomEvent) {
        let params = e.detail;
        if (params.startPoint) {
            params.startPoint = new CatBase.Country(params.startPoint);
        }
        // Просим трип-контроллер пропатчить, уведомит об изменениях он сам.
        this.$.tripController.updateTrip(params);
    }

    // Если есть uid, то загружаем этот объект.
    _queryParamsChanged(uid: string): void {
        if (!uid) return;
        this.$.tripController.loadTrip(uid)
            .then((res) => this.$.snackbar.show({message: "Путешествие загружено"}))
            .catch(
                (err) => {
                    this.$.snackbar.show({message: err});
                    console.log(err);
                });
                
    }

    _cmdSaveTrip(): void {
        this.$.tripController.persistTrip()
            // .then((trip) => this.trip = trip)
            .then((res) => this.$.snackbar.show({message: "Сохранено"}))
            .catch((err) => {
                console.error(err);
                this.$.snackbar.show({message: err});
            });        
    }

    _cmdCreateNewTrip(): void {
        this.$.tripController.createNewTrip();
        this.$.snackbar.show({message: "Новое путешествие создано"});
    }

    _cmdOverviewTrip(): void {
        Polymer.importHref(this.resolveUrl('../../elements/cat-trip-overview-dialog/cat-trip-overview-dialog.html'), 
        () => {
            this.$.tripOverviewDialog.trip = this.$.tripController.trip;
            this.$.tripOverviewDialog.show();
            },
        (e: Event) => alert(e),
        false
        );
    }

    _cmdSearchStateChanged(e: CustomEvent): void {
        this._isSearchEnabled = e.detail.enabled;
        if (this._isSearchEnabled) {
            this._updateMarkers(this.$.searchController.searchCriterias);
        }
        else {
            this.set('markers', []);
        }        
    }

    _routePageChanged(page: string): void {
        this.pageNum = parseInt(page || '1');
        if (!this.pageNum) {
            console.warn('Page num is not a number');
            this.pageNum = 1;
        };
    }

    _updateCurrenDaytActivities(splice: any, pageNum: number):void {
        if (!splice.base || !pageNum) return;
        
        this.set('currentDayActivities', []);
        this.set('currentDayActivities' , splice.base[pageNum -1]);
    }

    _updatePersistenMarkers(splice: any, googleMap: google.maps.Map) {
        // Убеждаемся, что объект гуглокарт существует, а значит api проинициализированы
        if (!googleMap) return;
        // Если путь такой, то просто при инициализации присвоили массив активностей
        if (splice.path == 'trip.days') {
            let days: CatBase.Activity[][] = splice.value;
            days.forEach(poiList => {
                let markers = poiList.map(poi => poi.toMarker( () => this.markerClickFunc(poi.uid)) );
                this.$.catMap.addPersistentMarkers(markers);
            });
        }

        if (splice.path.endsWith('.splices')) {
            let splices = splice.value.indexSplices as any[];
            splices.forEach(val => {
                let poiList: CatBase.Activity[] = val.removed;
                poiList.forEach(poi => this.$.catMap.removePersistentMarker(poi.uid));
                if (val.addedCount) {
                    let i = val.index;
                    let poi: CatBase.Activity = val.object[i];
                    this.$.catMap.addPersistentMarkers([poi.toMarker()]);
                }
                // let i = val.index;
                // let poi = val.object[i];
            });
        }
    }

    // Карты инициализировались, инициализируем все зависимости
    _initMapEventListeners(gMap: google.maps.Map) {
        if (!gMap) return;
        
        // Инициализируем гугло-провайдер, иначе api не работает.
        this.mapProvider.init(gMap);

        /* Подписываемся на события гугло-карт, т.к. нам надо на них реагировать */
        google.maps.event.addListener(gMap, 'idle', () => {

            // // Это центральный тестовый маркер для дебагинга позиционирования карты.
            // if (this._centerTag) this._centerTag.setMap(null);
            // this._centerTag = new google.maps.Marker({
            //     position: this.googleMap.getCenter(),
            //     map: this.googleMap
            // });
                    
            // Если изменился вьюпорт и зум не сильно далекий, то обновляем отметки, 
            // в соответствии со всеми выбранными тэгами
            // if (this.searchCriterias && this.searchCriterias.length > 0 && gMap.getZoom() >= this._maxLocalZoomLevel) {
            //     this._updateMarkers(this.searchCriterias);
            // }

            // // И меняем режим карты в зависимости от текущего зума (на этот режиме ориентируется механизм поиска)
            // if (gMap.getZoom() <= this._maxLocalZoomLevel && this.mapMode != 'global') {
            //     this.set('mapMode', 'global');
            //     console.log('mapMode changed to glob');
            // }
            // if (gMap.getZoom() > this._maxLocalZoomLevel && this.mapMode != 'local') {
            //     this.set('mapMode', 'local');

            //     // Добавляем на карту компонент, отображающий инфу о POI
            //     // this.$.catMap.injectComponent(this.$.poiDetailsContainer);
            //     console.log('mapMode changed to local');
            // }
                
        });

    }

    // Функция, которая вызывается при клике на маркере карты!
    markerClickFunc = (uid: string) => {
        this.mapProvider.getDetail(uid)
                            // информация о месте загружена, готовим компонент с инфой.
                            .then(info =>  {
                                this.$.poiDetails.data = info;
                                this._showPoiDetails();
                            });
    };


    /**
     * Заполняет маркеры и вешает на них события
     * @param {Array} criterias - массив критериев поиска
     */
    _updateMarkers(criterias: CatSearch.Criterias) {
        // Проверяем включен ли поиск
        if (!this._isSearchEnabled) return;
        // получаем результаты
        this.mapProvider.getData(criterias)
                // конвертируем в массив гугло-маркеров и передаем функцию, кот. вызывается по клику.
                .then(res => Utils.GoogleMap.convert2markers(res, pl => this.markerClickFunc(pl.place_id)))
                // апдейдим переменную с нотификацией зависимых компонентов (гуглокарт)
                .then((res) => this.set('markers', res))
                .catch(console.error);
    }

    /**
     * Выбрали что-то из предложенных результатов поиска
     * @param {Object} val 
     */
    _selectedSuggestionChanged(val: CatSearch.Criteria) {
        // иногда будет и null, т.к. элемент сбрасывая свое выбранное значение, ставит selected = -1.
        if (!val) return;

        this.$.searchController.pushCriteria(val);
        
        // По-особенному обрабатываем тэг страны, т.к. это опора наша.
        if (val.type == CatSearch.CriteriaType.COUNTRY) {
            // Сохраняем страну в целом объекте путешествий! И там дальше должна обновиться карта.
            let country = new CatBase.Country(val.displayName, val.value);
            this.$.tripController.updateTrip({country : country});
        }

        // и прячем контрол c подсказками
        this.$.suggestList.hidden = true;
        // Обновить маркеры тоже надо.
        this._updateMarkers(this.$.searchController.searchCriterias);
    }

    /**
     * Вызывается когда кликнули на кнопке добавить poi в перечень
     */
    _addPoi(e: CustomEvent) {
        let data = this.$.poiDetails.data;
        let latlng = {
            lat: data.geometry.location.lat(),
            lng: data.geometry.location.lng()
        };
        let activity = new CatBase.Activity( {
                                            displayName: data.name, 
                                            uid: data.place_id, 
                                            icon: data.icon,
                                            latlng});

        this.$.tripController.addPoi(this.pageNum - 1, activity);
        
        //this.$.catMap.addPersistentMarkers();
        this.$.snackbar.show({message: `"${data.name}" добавлена`});
    }

    /**
     * Вызывается, когда надо удалить POI
     * @param e
     */
    _onPoiRemove(e: CustomEvent) {
        let poi = e.detail.poi;
        let removed = this.$.tripController.removePoi(this.pageNum -1, poi.uid);
        this.$.snackbar.show({message: `POI удалена`});
    }

    // Обработчик события клика на активности
    _onPoiClicked(e: CustomEvent) {
        // Центрируем карту относительно POI
        let poi: CatBase.Activity = e.detail.poi;
        this.$.catMap.focusOnMarker(poi.uid);
        this.mapProvider.getDetail(poi.uid)
            .then(detail => {
                this.$.poiDetails.data = detail;
                this._showPoiDetails();
            });
    }


    /**
     * Произошли изменения в строке поиска - обновляем список подсказок
     * @param e 
     */
    _searchFieldChanged(e: CustomEvent) {
        let searchString = e.detail;
        this._searchProviders[this._mapMode](searchString)
               .then((res) => {
                   this.set('suggestions', []);
                   this.push('suggestions', ...res);
                })
               .then((res) => this.$.suggestList.hidden = false)
               .catch(console.error);
    }

    _searchFieldEntered(e: CustomEvent) {
        console.log(e);
    }

    _mapModeChanged(e: CustomEvent) {
        this._mapMode = e.detail.mode;
    }

    _mapBoundsChanged(e: CustomEvent) {
        let bounds = e.detail.bounds;
        this._updateMarkers(this.$.searchController.searchCriterias);
    }

    // Удаляем так - он же критерий поиска.
    _removeTag(e: CustomEvent) {
        let tag:CatSearch.Criteria = e.detail;
        this.$.searchController.removeCriteria(tag);
        this._updateMarkers(this.$.searchController.searchCriterias);
    }

    _hidePoiDetails() {
        Velocity(this.$.poiDetailsContainer, {right: `-${this.$.poiDetailsContainer.offsetWidth}px`});
    }

    _showPoiDetails() {
        Velocity(this.$.poiDetailsContainer, {right: `0px`});
    }
}

customElements.define(CatTrip.is, CatTrip);
