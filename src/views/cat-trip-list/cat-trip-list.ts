class CatTripList extends Polymer.Element {
    static get is() {return 'cat-trip-list'; }


    static get properties() { return {
        tripProvider: Object,
        items: Array,
        pageTitle: String,
        btnText: String,
        showPrivateOnly: {
            type: Boolean,
            value: false,
        }
    }} // properties

    // 4TS
    items: CatBase.Trip[];
    tripProvider: CatDataProvider.TripDataProvider;
    showPrivateOnly: boolean;

    connectedCallback() {
        super.connectedCallback();
        this.init();
    }

    init() {
        this.initData();
    }

    initData() {
         if (this.showPrivateOnly){
            this.tripProvider.getUserTripsMeta()
                .then(res => {
                        let retVal = res;
                        res.forEach(trip => trip.img = trip.img || `http://loremflickr.com/100/100/trip,adventure?random=${Math.random()}`);
                        return retVal;
                    })
                .then(res => this.set('items', res));
        }
        else {
            // let tc = new CatBase.TripController(null);
            let items = new Array(10).fill({}).map(el => new CatBase.Trip());

            items.forEach(el => el.img = `http://loremflickr.com/100/100/trip,adventure?random=${Math.random()}`);
            this.set('items', items);
        }
    }


} // class

customElements.define(CatTripList.is, CatTripList);
