// Promise<T> doesn't use the type T as a member and only in functions. 
// The function type compatability rules allow the promise's to be compatible in this case.
// FIX: https://basarat.gitbooks.io/typescript/content/docs/tips/promise-safety.html
interface Promise<T>{
  _ensureTypeSafety: T;
}

declare var dateFns: any;
declare var Swiper: any;
declare var Sortable: any;
declare var Velocity: any;

declare var fixture: any;
declare var flush: any;

declare class MarkerClusterer {
  constructor(map: google.maps.Map,
              markers?: google.maps.Marker[],
              opts?: MarkerClustererOptions);
  /**
   * Add an array of markers to the clusterer.
   */
  addMarkers: (markers: google.maps.Marker[], nodraw?: boolean) => void;
  /**
   * Clears all clusters and markers from the clusterer.
   */
  clearMarkers: () => void;
}

declare type MarkerClustererOptions = {
  imagePath?: string,
  gridSize?: number,
  maxZoom?: number, 
  zoomOnClick?: boolean,
  averageCenter?: boolean,
  minimumClusterSize?: number,
  styles?: {
    url?: string,
    height?: number,
    width?: number,
    anchor?: any[],
    textColor?: string,
    textSize?: number,
    backgroundPosition?: string
  }
    
}